#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
  int data;
  struct Node *next;
} Node;

Node *front = NULL;
Node *rear = NULL;

Node *node(int data);
int traverse(int is_print_data);
void *enqueue(int data);
void *dequeue();
Node *get_front();
Node *get_rear();

Node *node(int data)
{
  Node *node = (Node *)malloc(sizeof(Node));
  node->data = data;
  node->next = NULL;
  return node;
}

int traverse(int is_print_data)
{
  int len = 0;
  Node *current_node = front;
  while (current_node != NULL)
  {
    if (is_print_data)
      printf("%d -> ", current_node->data);
    current_node = current_node->next;
    len++;
  }
  printf("\n");
  return len;
}

void *enqueue(int data)
{
  Node *new_node = node(data);

  if (front == NULL)
  {
    front = rear = new_node;
    return NULL;
  }

  rear->next = new_node;
  rear = new_node;
}

void *dequeue()
{
  Node *front_node = front;
  front = front_node->next;
  free(front_node);
}

Node *get_front()
{
  return front;
}

Node *get_rear()
{
  return rear;
}
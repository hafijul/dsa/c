#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    int data;
    struct Node *left, *right;

} Node;

Node *node(int data);
Node *insert(Node *root, int data);
Node *inorder_traversal(Node *root);
Node *preorder_traversal(Node *root);
Node *postorder_traversal(Node *root);

Node *node(int data)
{
    Node *node = (Node *)malloc(sizeof(Node));
    node->data = data;
    node->left = node->right = NULL;
    return node;
}

Node *insert(Node *root, int data)
{
    if (!root)
        return node(data);
    if (root->data < data)
        root->right = insert(root->right, data);
    else if (root->data > data)
        root->left = insert(root->left, data);
    return root;
}

Node *inorder_traversal(Node *root)
{
    if (!root)
        return NULL;
    inorder_traversal(root->left);
    printf("%d -> ", root->data);
    inorder_traversal(root->right);
}

Node *preorder_traversal(Node *root)
{
    if (!root)
        return NULL;
    printf("%d -> ", root->data);
    inorder_traversal(root->left);
    inorder_traversal(root->right);
}

Node *postorder_traversal(Node *root)
{
    if (!root)
        return NULL;
    inorder_traversal(root->left);
    inorder_traversal(root->right);
    printf("%d -> ", root->data);
}

int main()
{

    Node *root = NULL;
    root = insert(root, 5);
    root = insert(root, 6);
    root = insert(root, 11);
    root = insert(root, 9);
    root = insert(root, 3);
    root = insert(root, 0);
    root = insert(root, 8);

    inorder_traversal(root);
    printf("\n-----------------------------------\n");

    postorder_traversal(root);
    printf("\n-----------------------------------\n");

    preorder_traversal(root);
    free(root);
    return 0;
}
